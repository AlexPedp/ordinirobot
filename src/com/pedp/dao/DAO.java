package com.pedp.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import com.pedp.data.Coordinate;
import com.pedp.util.Logger;

public class DAO {

	private static final Logger logger = Logger.getLogger(DAO.class);

	private static final String TAG = "DAO";


	public static final boolean USE_SQLITE = true;

	static final String TABLE_COORDINATES = "COORDINATES";
	 

	private DatabaseManager databaseManager;

	public DAO(){}

	public void setDatabaseManager(DatabaseManager dm){
		this.databaseManager = dm;
	}
	
	public void init() {
		if (USE_SQLITE) {
			Connection conn = databaseManager.getConnection();
			// creiamo le tabelle
			try {
				try {
					Statement stat;
					stat = conn.createStatement();
					// stat.executeUpdate("drop table if exists people;");
					stat.executeUpdate("CREATE TABLE " + TABLE_COORDINATES
							+ " (" + "ID INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL, "
							+ "AZIENDA TEXT," + "CLIENTE TEXT, " + "PAGE INTEGER, " + "KEY TEXT, "
							+ "X INTEGER, " + "Y INTEGER, "
							+ "WIDTH INTEGER, " + "HEIGHT INTEGER, "
							+ "DATA TIMESTAMP, " + "VERSION TEXT)");
							 

				} catch (SQLException e) {
					// mLogger.e(TAG, e.getMessage());
					// ignore,
					// table already exists...
				}
 

			} finally {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}


	}

	public boolean insertCoordinates(Coordinate coordinate) {
		Connection connection = null;
		boolean insert = false;
		try {
			connection = databaseManager.getConnection();
			insert = DAOCoordinates.insert(connection, coordinate);
		} finally {
			silentClose(connection);
		}
		return insert;
	}
	
	public List<Coordinate> findAllByAzienda(String azienda, String cliente, int page) {
		Connection connection = null;
		 List<Coordinate> findAllByAzienda;
		try {
			connection = databaseManager.getConnection();
			  findAllByAzienda = DAOCoordinates.findAllByAzienda(connection, azienda, cliente, page);
		} finally {
			silentClose(connection);
		}
		return findAllByAzienda;
	}
	
 
	
	public boolean deleteAllCoordinates(String azienda) {
		Connection connection = null;
		boolean delete = false;
		try {
			connection = databaseManager.getConnection();
			delete = DAOCoordinates.deleteAllCoordinates(connection, azienda);
		} finally {
			silentClose(connection);
		}
		return delete;
	}

	
	public static DAO create(DatabaseManager manager){
		DAO dao = new DAO();
		dao.setDatabaseManager(manager);
		return dao;
	}
	
	public static void silentClose(Connection conn) {
		try {
			if (conn != null)
				conn.close();
		} catch (Exception e) {
			// ignore
		}
	}


}
