package com.pedp.data;

public class Riga {

	public String cliente;
	public String numero;
	public String data;
	public String spedizioneName;
	public String spedizioneAddress;
	public String spedizioneCity;
	public String consegna;
	public String articolo;
	public String descrizione;
	public String quantita;
	public String prezzo;
	public String um;
	public String rifArticolo;
	public String magazzino;
	public String vettore;
 
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	 
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getConsegna() {
		return consegna;
	}
	public void setConsegna(String consegna) {
		this.consegna = consegna;
	}
	public String getArticolo() {
		return articolo;
	}
	public void setArticolo(String articolo) {
		this.articolo = articolo;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public String getQuantita() {
		return quantita;
	}
	public void setQuantita(String quantita) {
		this.quantita = quantita;
	}
	public String getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(String prezzo) {
		this.prezzo = prezzo;
	}
	public String getSpedizioneName() {
		return spedizioneName;
	}
	public void setSpedizioneName(String spedizioneName) {
		this.spedizioneName = spedizioneName;
	}
	public String getSpedizioneAddress() {
		return spedizioneAddress;
	}
	public void setSpedizioneAddress(String spedizioneAddress) {
		this.spedizioneAddress = spedizioneAddress;
	}
	public String getSpedizioneCity() {
		return spedizioneCity;
	}
	public void setSpedizioneCity(String spedizioneCity) {
		this.spedizioneCity = spedizioneCity;
	}
	public String getMagazzino() {
		return magazzino;
	}
	public void setMagazzino(String magazzino) {
		this.magazzino = magazzino;
	}
	public String getVettore() {
		return vettore;
	}
	public void setVettore(String vettore) {
		this.vettore = vettore;
	}
	public String getRifArticolo() {
		return rifArticolo;
	}
	public void setRifArticolo(String rifArticolo) {
		this.rifArticolo = rifArticolo;
	}
	public String getUm() {
		if (um!=null && !um.isEmpty()){
			return um;
		 }else{
			 return "";
		}
					
	}
	public void setUm(String um) {
		this.um = um;
	}
	
}
