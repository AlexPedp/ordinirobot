package com.pedp.rectangle;

import java.awt.geom.Rectangle2D;
import java.util.LinkedHashMap;

import com.pedp.data.Chiave;
import com.pedp.testo.Testo;

public class PDFRectangle {


	private LinkedHashMap<Chiave, Rectangle2D> rectangles = new LinkedHashMap<Chiave, Rectangle2D>();
	private String azienda;

	public PDFRectangle(String azienda){
		this.azienda = azienda;
		init();
	}

	public void init(){

		switch (azienda) {
		case Testo.AZIENDA_SOLIGO:
			RectangleSol rectSol = new RectangleSol();
			rectangles = rectSol.getRectangles();
		case Testo.AZIENDA_COPEGO:
			RectangleCop rectCop = new RectangleCop();
			break;
		case Testo.AZIENDA_AMBROSI:
			RectangleAmb rectAmb = new RectangleAmb();
			rectangles = rectAmb.getRectangles();
			break;
		case Testo.AZIENDA_AMBROSI_USA:
			RectangleAfu rectAfu = new RectangleAfu();
			rectangles = rectAfu.getRectangles();
			break;
		default:
			break;
		}
		 
	}
 

 
	public Rectangle2D rectByType(String type, int page){
		String name = type + "_" + page;
		Rectangle2D rectangle = rectangles.get(name);

		if (rectangle == null) {
			name = type;
			rectangle = rectangles.get(name);
		}
		return rectangle;
	}

	public LinkedHashMap<Chiave, Rectangle2D> getRectangles() {
		return rectangles;
	}
 
 
	
}

