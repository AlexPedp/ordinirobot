package com.pedp.rectangle;

import java.awt.geom.Rectangle2D;
import java.util.LinkedHashMap;

import com.pedp.data.Chiave;
import com.pedp.testo.Testo;

public class RectangleAmb {

private LinkedHashMap<Chiave, Rectangle2D> rectangles = new LinkedHashMap<Chiave, Rectangle2D>();
	
	public RectangleAmb(){
		init();
	}
	
	public void init(){
		CARREFOUR();
		COOP();
	}
	
	public LinkedHashMap<Chiave, Rectangle2D> getRectangles(){
		return rectangles;
	}
	
public void CARREFOUR(){
		
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CARREFOUR);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(350, 50, 300, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CARREFOUR);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(500, 150, 300, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CARREFOUR);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 10, 1000, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CARREFOUR);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 230, 2000, 500);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CARREFOUR);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CARREFOUR);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
 		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CARREFOUR);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CARREFOUR);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 240, 2000, 500);
		add(chiave, rect);
		 
	}
	
	public void COOP(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.COOP);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(0, 220, 1000, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.COOP);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(20, 230, 1000, 100);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.COOP);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 200, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.COOP);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 375, 2000, 500);
		add(chiave, rect);
	 
		 
	}
	
	public void add(Chiave chiave, Rectangle2D rect){
		rectangles.put(chiave, rect);
	}
	
}
