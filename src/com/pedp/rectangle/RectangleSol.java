package com.pedp.rectangle;

import java.awt.geom.Rectangle2D;
import java.util.LinkedHashMap;

import com.pedp.data.Chiave;
import com.pedp.testo.Testo;

public class RectangleSol {

	
	private LinkedHashMap<Chiave, Rectangle2D> rectangles = new LinkedHashMap<Chiave, Rectangle2D>();
	
	public RectangleSol(){
		init();
	}
	
	public void init(){
			GUARNIER();
			UNICOMM();
			CADORO();
			CONAD();
			OTTAVIAN();
			GARDERE();
			APULIA();
	}
	
	public LinkedHashMap<Chiave, Rectangle2D> getRectangles(){
		return rectangles;
	}
	
	
	public void GUARNIER(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GUARNIER);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(20, 100, 115, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GUARNIER);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double( 460, 100, 100, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GUARNIER);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(100, 160, 500, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GUARNIER);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 250, 2000, 300);
//		rect = new Rectangle2D.Double(0, 290, 2000, 300);
		add(chiave, rect);
		
	}
	
	public void UNICOMM(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.UNICOMM);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(20, 100, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.UNICOMM);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double( 460, 100, 100, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.UNICOMM);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(100, 160, 500, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.UNICOMM);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 250, 2000, 300);
//		rect = new Rectangle2D.Double(0, 290, 2000, 300);
		add(chiave, rect);
		
	}
	
	public void CADORO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(90, 220, 40, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
 		rect = new Rectangle2D.Double(0, 10, 1000, 100);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 10, 1000, 100);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 270, 2000, 500);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
 		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 2000, 500);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
 		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 2000, 500);
		add(chiave, rect);
		 
	}
	
	public void CONAD(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CONAD);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(260, 10, 220, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CONAD);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
 		rect = new Rectangle2D.Double(0, 210, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CONAD);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 200, 1000, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CONAD);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 360, 2000, 100);
		add(chiave, rect);
		 
	}
	
	public void OTTAVIAN(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.OTTAVIAN);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(0, 120, 500, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.OTTAVIAN);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 230, 500, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.OTTAVIAN);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 150, 1000, 300);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.OTTAVIAN);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 260, 2000, 300);
		add(chiave, rect);
	 
		 
	}
	
	public void GARDERE(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDERE);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(0, 100, 1000, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDERE);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 200, 2000, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDERE);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 15, 1000, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDERE);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 250, 2000, 200);
		add(chiave, rect);
	 
		 
	}
 
	
	
	public void APULIA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.APULIA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(0, 200, 500, 100);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.APULIA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 300, 500, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.APULIA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 300, 500, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.APULIA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 350, 2000, 300);
		add(chiave, rect);
	 
		 
	}
	
	public void add(Chiave chiave, Rectangle2D rect){
		rectangles.put(chiave, rect);
	}
	
}
