package com.pedp.junit;

import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.text.PDFTextStripperByArea;

import com.pedp.util.Logger;

public class TestClass {
	
	private static final Logger logger = Logger.getLogger(TestClass.class);
	private static final String TAG = "TestClass";
	
	public static void main(String[] args) throws IOException {
		
		PDDocument doc = PDDocument.load(new File("document2017-03-07-101931.PDF"));
		PDPageTree pages = doc.getPages();
		int count = pages.getCount();
		PDPage page = pages.get(1);
		
		String s = getText(page, 0, 0, 2000, 1000);
		logger.info(TAG, "RIGHE: " + s);
//		String type = null;
//		PDDocument doc = PDDocument.load(new File("document2017-03-07-104838.PDF"));
//		String s =  new PDFTextStripper().getText(doc);
//		logger.info(TAG, s);
		
	}
	
	private static String getText(PDPage page, int x, int y, int width, int height) throws IOException{
		Rectangle2D region = new Rectangle2D.Double(x, y, width, height);
		String regionName = "region";
		PDFTextStripperByArea stripper;
		stripper = new PDFTextStripperByArea();
 		stripper.setSortByPosition( true );
		stripper.addRegion(regionName, region);
		stripper.extractRegions(page);
		String s =  stripper.getTextForRegion("region");
		return s;

	}

}
