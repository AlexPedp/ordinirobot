package com.pedp.extract.txt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.pedp.testo.Testo;

public class ExtractTxt {

	private String ordine;

	public void execute(File file) throws IOException, ParseException{

		File csvFile = new File(Testo.SMA_FILE + "_" +UUID.randomUUID() + ".csv");
		PrintWriter pw = new PrintWriter(csvFile);
		StringBuilder sb = new StringBuilder();
		String delimiter = " ";
		List<String> contents= new ArrayList<String>();

		FileInputStream fs= new FileInputStream(file.getAbsoluteFile());
		BufferedReader br = new BufferedReader(new InputStreamReader(fs));
		String line;

		StringBuilder  lines = new StringBuilder();
		while ((line = br.readLine()) != null) {
			lines.append(line);
			contents.add(line);
		}

		ordine = "";

		int i = 0;
		for (String row: contents) {
			i++;

			if (i< 18){
				continue;
			}

			if (row.toUpperCase().trim().contains(Testo.TOTALE) || row.toUpperCase().trim().contains(Testo.SEPARATORE)){
				break;
			}
			if (row.toUpperCase().trim().contains(Testo.ZONA)){
				continue;
			}

			String art = row.substring(1, 11);
			String desc = row.substring(12, 30);
			String qta = row.substring(31, row.length());

			if (art.trim().isEmpty()){
				continue;
			}

			sb.append(Testo.SMA);
			sb.append(';');
			sb.append(Testo.SMA_DEPOSITO.trim());
			sb.append(';');
			sb.append(ordine);
			sb.append(';');
			sb.append(ordine);
			sb.append(';');
			sb.append(art.trim());
			sb.append(';'); 
			sb.append(desc.trim());
			sb.append(';');

			String strArray[] = qta.trim().split(delimiter);

			for (String s: strArray) {
				if (!s.isEmpty()){
					sb.append(s);
					sb.append(';'); 
				}
			}
			sb.append('\n');

			System.out.println(line);
		}

		if (br != null){
			br.close();
		}

		pw.write(sb.toString());
		pw.close();
		System.out.println("Done");

	}

	 


}


