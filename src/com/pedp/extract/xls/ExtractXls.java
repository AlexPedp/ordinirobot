package com.pedp.extract.xls;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import com.pedp.data.Riga;
import com.pedp.testo.Testo;


public class ExtractXls {

	private String deposito;
	private String numero;
	private String data;
	private String consegna;
	private String art;
	private String desc;
	private String mag;
	private String vet;
	private String colli;
	private String type;
	private String rifArt;
	private Riga riga;

	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	public void execute(String inputPath, File file) throws IOException{

		List<Riga> rows = new ArrayList<Riga>();

		Workbook workbook = null;
		try {

			workbook = Workbook.getWorkbook(file);
			Sheet sheet = workbook.getSheet(0);

			int startIndex = findStartIndex(sheet);

			Cell cell1 = sheet.getCell(0, 3);
			String [] typeArray = cell1.getContents().toUpperCase().split("\\s+");
			type = typeArray[0].toUpperCase();

			if (type == null || type.isEmpty()){
				System.out.println("Ordine di cliente non valido");
				return ;
			}

			Cell cell2 = sheet.getCell(21, 11);
			deposito =  cell2.getContents();

			Cell cell3 = sheet.getCell(0, startIndex);
			numero = cell3.getContents();
			if (numero.isEmpty()){
				cell3 = sheet.getCell(0, startIndex-1);
				numero = cell3.getContents();
			}

			Cell cell4 = sheet.getCell(26, 4);
			data =  cell4.getContents();
			if (data.isEmpty()){
				cell3 = sheet.getCell(26, 3);
				data = cell4.getContents();
			}
			
			String newData = convertDate(data, "dd/MM/yy", "dd/MM/yyyy");
			if (newData == null){
				newData = convertDate(data, "MM/dd/yyyy", "dd/MM/yyyy");
			}
			
			data = newData;
		
			Cell cell5 = sheet.getCell(9, startIndex);
			vet =  cell5.getContents();

			Cell cell6 = sheet.getCell(17, startIndex);
			consegna =  cell6.getContents();
			if (consegna.isEmpty()){
				cell6 = sheet.getCell(17, startIndex-1);
				consegna = cell6.getContents();
			}
			if (!consegna.isEmpty()){
				String[] arr = consegna.split("/");
				String day = String.format("%02d", Integer.parseInt(arr[1]));
				String month = String.format("%02d", Integer.parseInt(arr[0]));
				String year = arr[2];
				consegna = day + "/" + month + "/" + year;
				consegna = convertDate(consegna, "dd/MM/yy", "dd/MM/yyyy");
			}

			for (int row = 0 ; row < sheet.getRows(); row ++ ) {

				if(row < (startIndex+3)){
					continue;
				}

				if (art == null || art.isEmpty()){
					art = sheet.getCell(0, row).getContents();	
				}
				if (colli == null || colli.isEmpty()){
					colli = sheet.getCell(11, row).getContents();	
				}
				if (mag == null || mag.isEmpty()){
					mag = sheet.getCell(19, row).getContents();	
				}
				if (desc == null || desc.isEmpty()){
					desc = sheet.getCell(1, row).getContents();
				}
				if (rifArt == null || rifArt.isEmpty()){
					rifArt = sheet.getCell(25, row).getContents();
				}

				if (desc != null && desc.length() > 0) {
					riga = new Riga();
					riga.setCliente(type);
					String[] parts = deposito.split("\n");
					riga.setSpedizioneName(parts[0]);
					riga.setSpedizioneAddress(parts[1]);
					riga.setSpedizioneCity(parts[2]);
					riga.setNumero(numero);
					riga.setData(data);
					riga.setConsegna(consegna);
					riga.setArticolo(art);
					riga.setDescrizione(desc);
					riga.setQuantita(colli);
					riga.setPrezzo("0,0");
					riga.setUm("");
					riga.setMagazzino(mag);
					riga.setVettore(vet);
					riga.setRifArticolo(rifArt);
					rows.add(riga);
					//					System.out.println(art + "|" + desc+ "|" + rifArt + "|" + colli + "|" + mag);
					clear();
				}

			}



		} catch (IOException e) {
			e.printStackTrace();
		} catch (BiffException e) {
			e.printStackTrace();
		} finally {

			if (workbook != null) {
				workbook.close();
			}

		}

		createCSV(inputPath, file, rows);

	}

	public void clear(){
		art = null;
		colli = null;
		mag = null;
		desc = null;
		rifArt = null;
	}


	public void createCSV(String inputPath, File file, List<Riga> rows) throws FileNotFoundException{

		String fname = file.getName();
		int pos = fname.lastIndexOf(".");
		if (pos > 0) {
			fname = fname.substring(0, pos);
		}

		File csvFile = new File(inputPath + type + "_" +UUID.randomUUID() + ".csv");

		PrintWriter pw = new PrintWriter(csvFile);
		StringBuilder sb = new StringBuilder();


		for (Riga r: rows) {
			sb.append(r.getCliente().trim());
			sb.append(";");
			sb.append(r.getSpedizioneName().trim());
			sb.append(";");
			sb.append(r.getSpedizioneAddress().trim());
			sb.append(";");
			sb.append(r.getSpedizioneCity().trim());
			sb.append(";");
			sb.append(r.getNumero().trim());
			sb.append(";");
			sb.append(r.getData().trim());
			sb.append(";");
			sb.append(r.getConsegna().trim());
			sb.append(";");
			sb.append(r.getArticolo().trim());
			sb.append(";"); 
			sb.append(r.getDescrizione());
			sb.append(";"); 
			sb.append(r.getQuantita().trim());
			sb.append(";");
			sb.append(r.getPrezzo().trim());
			sb.append(";");
			sb.append(r.getUm());
			sb.append(";");
			sb.append(r.getRifArticolo());
			sb.append(";");
			sb.append(r.getMagazzino());
			sb.append(";");
			sb.append(r.getVettore());
			sb.append(";");
			sb.append("\n");		
		}


		pw.write(sb.toString());
		pw.close();

	}

	public int findStartIndex(Sheet sheet){
		int index = 0;
		for (int row=1; row < sheet.getRows();row++) {
			Cell cell1 = sheet.getCell(0, row);
			if (cell1.getContents().toUpperCase().contains(Testo.CUSTOMER_PO)){
				index = row+1;
				return index;
			}
		}
		return index; 
	}

	public String convertDate(String dateFrom, String fromFormat, String toFormat){
		String dateTo=null;
		try {
			SimpleDateFormat sdfFrom = new SimpleDateFormat(fromFormat);
			sdfFrom.setLenient(false);
			SimpleDateFormat sdfTo = new SimpleDateFormat(toFormat);
			sdfTo.setLenient(false);
			Date date;
			date = sdfFrom.parse(dateFrom);
			dateTo = sdfTo.format(date);
		} catch (Exception e) {

		}
		return dateTo; 
	}


}


