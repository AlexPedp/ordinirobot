package com.pedp.extract.pdf;

import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;

import com.pedp.data.Chiave;
import com.pedp.data.Coordinate;
import com.pedp.data.Riga;
import com.pedp.logic.Parsers;
import com.pedp.main.Application;
import com.pedp.rectangle.PDFRectangle;
import com.pedp.testo.Testo;
import com.pedp.util.Logger;

public class ExtractPdf {

	private static final Logger logger = Logger.getLogger(ExtractPdf.class);
	private static final String TAG = "ExtractPDF";

	private String cliente;
	private String spedizione ="";
	private String consegna ="";
	private String ordine ="";
	private String ordineStr ="";
	private String data ="";
	private String righe ="";

	private Riga row;
	private List<Riga> rows = new ArrayList<Riga>();

	public void init(String azienda) {
		Application.getInstance().getDao().deleteAllCoordinates(azienda);
		PDFRectangle pdfr = new PDFRectangle(azienda);
		LinkedHashMap<Chiave, Rectangle2D> rectangles = pdfr.getRectangles();

		for (Map.Entry<Chiave, Rectangle2D> entry : rectangles.entrySet()) {
			Chiave key = entry.getKey();
			Rectangle2D value = entry.getValue();

			Coordinate c = new Coordinate();
			c.setAzienda(azienda);
			c.setKey(key.getKey());
			c.setCliente(key.getCliente());
			c.setPage(key.getPage());
			c.setVersion(key.getVersion());

			Double x = value.getX();
			Double y = value.getY();
			Double width = value.getWidth();
			Double height = value.getHeight();

			c.setX(x.intValue());
			c.setY(y.intValue());
			c.setWidth(width.intValue());
			c.setHeight(height.intValue());

			Application.getInstance().getDao().insertCoordinates(c);

		} 


	}

	public void execute(String inputPath, File file, String azienda) throws IOException, ParseException{

		cliente = getType(file);

		if (cliente == null){
			logger.error(TAG,"Ordine di cliente non valido");
			return;
		}

		logger.info(TAG,"Ordine del cliente: " + cliente);

		try(PDDocument doc = PDDocument.load(file)) {

			PDPageTree pages = doc.getPages();
			int count = pages.getCount();

			for (int i = 0; i < count;  i++) {
				List<Coordinate> coordinates = Application.getInstance().getDao().findAllByAzienda(azienda, cliente, i);

				PDPage page = pages.get(i); 


				for(int x = 0 ; x < coordinates.size() ; x++){
					Coordinate c =  coordinates.get(x);

					if (c.getX()==0 && c.getY()==0 && c.getWidth()==0 && c.getHeight()==0){
						continue;
					}

					String valueOf1 = String.valueOf(c.getX());
					String valueOf2 = String.valueOf(c.getY());
					String valueOf3 = String.valueOf(c.getWidth());
					String valueOf4 = String.valueOf(c.getHeight());
					String[] dati = new String[2];

					switch (x) {
					case 0:
						spedizione = getText(page, c.getX(), c.getY(), c.getWidth(), c.getHeight(), c.getVersion());
						logger.info(TAG, valueOf1 + "," +valueOf2 + "," + valueOf3 + "," + valueOf4);
						logger.info(TAG, "SPEDIZIONE: "+ spedizione);
						spedizione = Parsers.extractString(cliente, Testo.SPEDIZIONE, spedizione);
						break;
					case 1:
						consegna = getText(page, c.getX(), c.getY(), c.getWidth(), c.getHeight(), c.getVersion());
						logger.info(TAG, valueOf1 + "," +valueOf2 + "," + valueOf3 + "," + valueOf4);
						logger.info(TAG, "DATA CONSEGNA: " + consegna);
						dati = Parsers.extract(cliente, Testo.DATA_CONSEGNA, consegna);
						consegna = dati[0];
						break;
					case 2:
						ordineStr = getText(page, c.getX(), c.getY(), c.getWidth(), c.getHeight(), c.getVersion());
						logger.info(TAG, valueOf1 + "," +valueOf2 + "," + valueOf3 + "," + valueOf4);
						logger.info(TAG, "ORDINE: " + ordineStr);
						dati = Parsers.extract(cliente, Testo.ORDINE, ordineStr);
						ordine = dati[0];
						data = dati[1];
						break;
					case 3:
						righe += getText(page, c.getX(), c.getY(), c.getWidth(), c.getHeight(), c.getVersion());
						logger.info(TAG, valueOf1 + "," +valueOf2 + "," + valueOf3 + "," + valueOf4);
						logger.info(TAG, "RIGHE: " + righe);
						//					rows = readRows(righe);
						break;
					default:
						break;
					}

				}

			}

			rows = readRows(righe);

			createCSV(inputPath, file, rows);

		} catch (IOException e) {
			e.printStackTrace();
		}
//
//		file.delete();

	}

	private String getType(File pdfFile)  throws IOException{
		String type = null;

		try(PDDocument doc = PDDocument.load(pdfFile)) {
			//		PDDocument doc = PDDocument.load(pdfFile);

			String s =  new PDFTextStripper().getText(doc);

			for (String line : s.split("\n|\r")) {
				if (line.toUpperCase().trim().contains(Testo.AUCHAN)){
					return Testo.AUCHAN;
				} else if (line.toUpperCase().trim().contains(Testo.ESSELUNGA)) {
					return Testo.ESSELUNGA; 
				} else if (line.toUpperCase().trim().contains(Testo.CARREFOUR)) {
					return Testo.CARREFOUR; 
				} else if (line.toUpperCase().trim().contains(Testo.CARREFOUR)) {
					return Testo.CARREFOUR; 
				} else if (line.toUpperCase().trim().contains(Testo.FIORITAL)) {
					return Testo.FIORITAL; 
				} else if (line.toUpperCase().trim().contains(Testo.EUROSPIN)) {
					return Testo.EUROSPIN; 
				} else if (line.toUpperCase().trim().contains(Testo.SMA)) {
					return Testo.SMA; 
				} else if (line.toUpperCase().trim().contains(Testo.CENTRALE_PIVA)) {
					return Testo.CENTRALE; 
				} else if (line.toUpperCase().trim().contains(Testo.CONAD_PIVA)) {
					return Testo.CONAD; 
				} else if (line.toUpperCase().trim().contains(Testo.IPER)) {
					return Testo.IPER; 
				} else if (line.toUpperCase().trim().contains(Testo.CADORO)) {
					return Testo.CADORO; 
				} else if (line.toUpperCase().trim().contains(Testo.GUARNIER)) {
					return Testo.GUARNIER; 
				}  else if (line.toUpperCase().trim().contains(Testo.OTTAVIAN)) {
					return Testo.OTTAVIAN; 
				} else if (line.toUpperCase().trim().contains(Testo.GARDERE)) {
					return Testo.GARDERE; 
				} else if (line.toUpperCase().trim().contains(Testo.APULIA)) {
					return Testo.APULIA; 
				}  else if (line.toUpperCase().trim().contains(Testo.COOP)) {
					return Testo.COOP; 
				} else if (line.toUpperCase().trim().contains(Testo.UNICOMM)) {
					return Testo.UNICOMM; 
				}  else if (line.toUpperCase().trim().contains(Testo.TONYS_FINE_FOOD)) {
					return Testo.TONYS_FINE_FOOD; 
				} 

			}

			return type;

		}

		catch (IOException e) {
			e.printStackTrace();
			return null;
		}

	}


	private static String getText(PDPage page, int x, int y, int width, int height, String version) throws IOException{
		Rectangle2D region = new Rectangle2D.Double(x, y, width, height);
		String regionName = "region";
		PDFTextStripperByArea stripper;
		stripper = new PDFTextStripperByArea();

		if (version.equals(Testo.V2)){
			stripper.setSortByPosition( true );
		}

		stripper.addRegion(regionName, region);
		stripper.extractRegions(page);
		String s =  stripper.getTextForRegion("region");
		return s;

	}


	public String extractNumber(final String str) {                

		if(str == null || str.isEmpty()) return "";

		StringBuilder sb = new StringBuilder();
		boolean found = false;
		for(char c : str.toCharArray()){
			if(Character.isDigit(c)){
				sb.append(c);
				found = true;
			} else if(found){
				break;                
			}
		}

		return sb.toString();
	}


	public void createCSV(String inputPath, File file, List<Riga> rows) throws FileNotFoundException{

		String fname = file.getName();
		int pos = fname.lastIndexOf(".");
		if (pos > 0) {
			fname = fname.substring(0, pos);
		}

		String[] clienteNew = cliente.trim().split(" ");
		File csvFile = new File(inputPath + clienteNew[0] + "_" +UUID.randomUUID() + ".csv");

		PrintWriter pw = new PrintWriter(csvFile);
		StringBuilder sb = new StringBuilder();


		for (Riga r: rows) {
			sb.append(r.getCliente().trim());
			sb.append(";");
			sb.append(r.getSpedizioneName().trim());
			sb.append(";");
			sb.append(r.getSpedizioneAddress().trim());
			sb.append(";");
			sb.append(r.getSpedizioneCity().trim());
			sb.append(";");
			sb.append(r.getNumero().trim());
			sb.append(";");
			sb.append(r.getData().trim());
			sb.append(";");
			sb.append(r.getConsegna().trim());
			sb.append(";");
			sb.append(r.getArticolo().trim());
			sb.append(";"); 
			sb.append(r.getDescrizione());
			sb.append(";"); 
			sb.append(r.getQuantita().trim());
			sb.append(";");
			sb.append(r.getPrezzo().trim());
			sb.append(";");
			sb.append(r.getUm());
			sb.append(";");
			sb.append("\n");		
		}


		pw.write(sb.toString());
		pw.close();

	}

	public List<Riga> readRows(String content){


		String delimiter = " ";
		String colli = "";

		List<Riga> rows = new ArrayList<Riga>();

		if (cliente == Testo.APULIA){
			content = content.replace("B2", "\r\n");
		}

		for (String line : content.split("\n|\r")) {
			if (!line.trim().isEmpty()) {

				if (line.toUpperCase().trim().contains(Testo.TOTALE) || 
						line.trim().contains(Testo.VALORIZZAZIONE) || 
						line.trim().contains(Testo.FINE_STAMPA) || 
						line.trim().contains(Testo.T_PAG)) {
					break;
				}

				String prezzo = "";
				if (cliente == Testo.CADORO) {

					if (line.contains(Testo.COSTO)){
						prezzo = line.substring(80, 92).trim();
						row.setPrezzo(prezzo);
						rows.add(row);
					}

					colli = line.substring(40, 45).trim();

					String extractNumber = extractNumber(colli);

					if (extractNumber.isEmpty()){
						continue;
					}
				}

				if (cliente == Testo.GUARNIER || cliente == Testo.UNICOMM) {

					if (line.contains(Testo.COSTO_KG) || line.contains(Testo.COSTO_PZ)){
						prezzo = line.substring(15, 26).trim();
						row.setPrezzo(prezzo);
						rows.add(row);
					}

					colli = line.substring(0, 4).trim();

					String extractNumber = extractNumber(colli);

					if (extractNumber.isEmpty()){
						continue;
					}
				}

				if (cliente == Testo.CONAD) {

					if (line.toUpperCase().contains(Testo.COMMISSIONE) || line.toUpperCase().contains(Testo.PERCENTO)){
						continue;
					}

					colli = line.substring(7, 9).trim();

					if (!colli.isEmpty()){
						prezzo = line.substring(0, 10).trim();
						row.setPrezzo(prezzo);
						rows.add(row);
					}

				}

				if (cliente == Testo.OTTAVIAN) {

					if (line.toUpperCase().contains(Testo.DESCRIZIONE)){
						continue;
					}

				}

				if (cliente == Testo.GARDERE) {

					if (line.toUpperCase().contains(Testo.DESCRIZIONE)){
						continue;
					}

				}

				if (cliente == Testo.APULIA) {

					if (line.toUpperCase().contains(Testo.ARTICOLO)){
						continue;
					}
				}


				if (cliente == Testo.CARREFOUR) {

					if (line.toUpperCase().contains(Testo.CARTON) || 
							line.toUpperCase().contains(Testo.NOTE) ||
							line.toUpperCase().contains(Testo.RESPONSABILE) ||
							line.toUpperCase().contains(Testo.CARREFOUR) ||
							line.toUpperCase().contains(Testo.PAG)){
						continue;
					}

				}

				if (cliente == Testo.COOP) {
					line = line.replaceAll(Testo.LINE, "");
					if (line.toUpperCase().contains(Testo.FATTURA) ||
							line.toUpperCase().contains(Testo.EXTRA) ) {
						continue;
					}

				}

				if (cliente == Testo.TONYS_FINE_FOOD) {
					int length = line.length();
					if (line.toUpperCase().contains(Testo.LINE_STR) || length < 20){					
						continue;
					}

				}

				try {

					String art = null;
					String desc = null;
					String qta = null;
					String um = null;
					String rowArray[]  = null;

					int indexOf=0;
					switch (cliente) {
					case Testo.FIORITAL:
						art = line.substring(0, 7);
						indexOf = line.indexOf("AL");
						desc = line .substring(8, indexOf+2);
						qta = line.substring(indexOf+2, line.length());
						break;
					case Testo.CARREFOUR:
						line = line.replaceAll(" 01", " ");
						rowArray = line.trim().split(" ");
						art = rowArray[0];
						desc = rowArray[1]+ rowArray[2];
						qta = rowArray[rowArray.length-6];
						prezzo = rowArray[rowArray.length-3];
						break;
					case Testo.EUROSPIN:
						art = line.substring(0, 15).trim();
						desc = line .substring(30, 60).trim();
						qta = line.substring(60, line.length());
						break;
					case Testo.SMA:
						art = line.substring(0, 15).trim();
						desc = line .substring(30, 60).trim();
						qta = line.substring(60, line.length());
						break;
					case Testo.CENTRALE:
						art = line.substring(0, 9).trim();
						indexOf = line.indexOf("KG");
						int next = line.indexOf("KG", indexOf+1);
						if (next != -1){
							indexOf = next;
						}
						desc = line .substring(10, indexOf+2).trim();
						qta = line.substring(indexOf+2, line.length());
						break;
					case Testo.CONAD:
						art = line.substring(0, 9).trim();
						desc = line .substring(11, 51);
						qta = line.substring(52, 58).trim();
						break;
					case Testo.IPER:
						art = line.substring(0, 10).trim();
						indexOf = line.indexOf("ES");
						desc = line .substring(11, indexOf+4);
						qta = line.substring(indexOf+4, line.length());
						break;
					case Testo.CADORO:
						art = line.substring(0, 7).trim();
						desc = line .substring(8, 36).trim();
						qta = colli.trim();
						break;
					case Testo.GUARNIER:
					case Testo.UNICOMM:
						art = line.substring(6, 15).trim();
						desc = line .substring(17, 57);
						qta = line.substring(58, 67).trim();
						break;
					case Testo.OTTAVIAN:
						indexOf = line.indexOf("7GA");
						if (indexOf==-1){
							indexOf = line.indexOf("7BL");
						}
						if (indexOf==-1){
							indexOf = line.indexOf("524");
						}
						art = line .substring(indexOf, indexOf+11);
						desc = line .substring(6, 20).trim();

						String toEnd = line .substring(indexOf+11, line.length());
						rowArray = toEnd.trim().split("\\s+");
						qta = rowArray[3];
						prezzo = rowArray[4];
						um = rowArray[2];
						break;
					case Testo.GARDERE:
						art = line.substring(5, 15).trim();
						desc = line .substring(15, 50).trim();
						qta = line.substring(61, line.length());
						break;
					case Testo.APULIA:
						art = line.substring(0, 4).trim();
						indexOf = line.indexOf("DOP");
						desc = line .substring(4, indexOf+3);
						qta = line.substring(indexOf+4, indexOf+9) + " " + line.substring(indexOf+9, line.length());
						break;
					case Testo.COOP:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[2] + rowArray[3];

						if (rowArray[rowArray.length-1].equals(Testo.KILI)){
							qta = rowArray[rowArray.length-4];
							prezzo = rowArray[rowArray.length-3];
						} else {

							if (rowArray[rowArray.length-2].equals(Testo.P)){
								qta = rowArray[rowArray.length-4];
								prezzo = rowArray[rowArray.length-3];
							} else {
								qta = rowArray[rowArray.length-3];
								prezzo = rowArray[rowArray.length-2];
							}

						}

						break;
					case Testo.TONYS_FINE_FOOD:
						rowArray = line.trim().split("\\s+");
						art = rowArray[4];
						desc = rowArray[6] + rowArray[7] + rowArray[8]  + rowArray[9];;
						qta = rowArray[5];
						int l = rowArray.length;
						prezzo = rowArray[l-2];
						break;
					default:
						art = line.substring(0, 10);
						desc = line .substring(10, 30);
						qta = line.substring(40, line.length());
						break;
					}

					row = new Riga();
					row.setCliente(cliente);
					row.setSpedizioneName(spedizione.toUpperCase());
					row.setSpedizioneAddress("");
					row.setSpedizioneCity("");
					row.setNumero(ordine);
					row.setData(data);
					row.setConsegna(consegna);
					row.setArticolo(art);
					row.setDescrizione(desc);

					switch (cliente) {
					case Testo.CADORO:
					case Testo.GUARNIER:
					case Testo.UNICOMM:
					case Testo.CONAD:
						row.setQuantita(qta);
						break;
					case Testo.CARREFOUR:
					case Testo.COOP:
					case Testo.OTTAVIAN:
						row.setQuantita(qta);
						row.setPrezzo(prezzo);
						row.setUm(um);
						rows.add(row);
						break;
					case Testo.TONYS_FINE_FOOD:
						row.setQuantita(qta);
						row.setPrezzo(prezzo);
						row.setUm(um);
						rows.add(row);
						break;
					default:

						String strArray[] = qta.trim().split(delimiter);
						int i = 0;
						for (String s: strArray) {
							if (!s.isEmpty()){
								i++;
								switch (i) {
								case 1:
									switch (cliente) {
									case Testo.GARDERE:
										row.setQuantita(s);;
										break;
									case Testo.APULIA:
										row.setPrezzo(s);
										break;
									default:
										break;
									}
									break;
								case 2:
									switch (cliente) {
									case Testo.APULIA:
										row.setQuantita(s);
										break;
									default:
										break;
									}

									break;
								case 3:
									switch (cliente) {
									case Testo.GARDERE:
										row.setPrezzo(s);;
										break;
									default:
										break;
									}
									break;
								case 4:
									break;
								case 5:
									break;
								case 6:
									break;
								case 7:
									break;
								case 8:

									break;
								case 9:

									break;
								default:
									break;
								}
							}
						}
						rows.add(row);
						break;
					}


				} catch (Exception e){

				}


			}

		}

		return rows;

	}



	public String [] parser(String line, String regex){
		Matcher m = Pattern.compile(regex).matcher(line);
		String [] dati =  new String[3];
		if (m.find()) {
			logger.info(TAG, m.group(0));
			logger.info(TAG, m.group(1));
			logger.info(TAG, m.group(2));
			logger.info(TAG, m.group(3));

			dati[0] = m.group(1);
			dati[1] = m.group(3);
			dati[2] = m.group(2);
		}


		return dati;

	}


}
