package com.pedp.logic;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;



public class Reader {

	private static final String SEMAFORO = "import.sem";

	public void run(String azienda, String inputPath, String backupPath) throws IOException, ParseException{

		if (!backupPath.endsWith("\\")){
			backupPath = backupPath + "\\";
		}

		File sem = new File(inputPath +  SEMAFORO);
		sem.createNewFile();


		File dir = new File(inputPath);
		File finder[] = dir.listFiles(new FileExtensionFilter(".pdf", ".txt", ".xls", ".xlsx", ".doc"));

		for (File file: finder){

			if (file.isDirectory()){
				continue;
			}


			Extract extract = new Extract();
			extract.run(file, azienda,inputPath);
			Path src = Paths.get(file.getAbsolutePath());
			Path dst = Paths.get(backupPath + file.getName());

			if (!Files.exists(Paths.get(backupPath))) {
				try {
					Files.createDirectories(dst);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			Files.copy(src, dst, StandardCopyOption.REPLACE_EXISTING);
			file.delete();
		}

		sem.delete();

	}


}
