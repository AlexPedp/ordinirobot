package com.pedp.logic;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;

import com.pedp.extract.doc.ExtractDoc;
import com.pedp.extract.pdf.ExtractPdf;
import com.pedp.extract.txt.ExtractTxt;
import com.pedp.extract.xls.ExtractXls;
import com.pedp.extract.xlsx.ExtractXlsx;



public class Extract {

	private final String TXT = "txt";
	private final String PDF = "pdf";
	private final String XLSX = "xlsx";
	private final String XLS = "xls";
	private final String DOC = "doc";
	
	public void run(File file, String azienda, String inputPath) throws IOException, ParseException{
		
		String extension  = getFileExtension(file);

		switch (extension.toLowerCase()) {
		case TXT:
			ExtractTxt txt = new ExtractTxt();
			txt.execute(file);
			break;
		case PDF:
			ExtractPdf pdf = new ExtractPdf();
			pdf.init(azienda);
			pdf.execute(inputPath, file, azienda);
			break;
		case XLSX:
			ExtractXlsx xlsx = new ExtractXlsx();
			xlsx.execute(file);
			break;
		case XLS:
			ExtractXls xls = new ExtractXls();
			xls.execute(inputPath, file);
			break;
		case DOC:
			ExtractDoc doc = new ExtractDoc();
			doc.execute(file);
			break;
		default:
			break;
		}

	}
	
	
	private String getFileExtension(File file) {
	    String name = file.getName();
	    try {
	        return name.substring(name.lastIndexOf(".") + 1);
	    } catch (Exception e) {
	        return "";
	    }
	}

}
