package com.pedp.main;

import com.pedp.util.Logger;

public class Main {

	private static final Logger logger = Logger.getLogger(Main.class);
	private static final String TAG = "Main";
	
	public static void main(String[] args) {
		  
		if (args.length == 0) {
			logger.error(TAG, "Azienda non esistente");
			System.exit(3);
		}
		  
		Application.launch(Application.class, args);
	}
	
}
