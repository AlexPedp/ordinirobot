package com.pedp.main;

import org.jdesktop.application.SingleFrameApplication;

import com.pedp.dao.DAO;
import com.pedp.dao.DatabaseManager;
import com.pedp.logic.Reader;
 

public class Application extends SingleFrameApplication{

	private DatabaseManager databaseManager;
	private DAO dao;
	private String azienda;
	private String inputPath;
	private String backupPath;
	
	public void init()  {
		
		databaseManager = DatabaseManager.getInstance();
		dao = DAO.create(databaseManager);
		databaseManager.init();
		dao.init(); // after db
		
		try {
			Reader reader = new Reader();
			reader.run(azienda, inputPath, backupPath);
		} catch (Exception e) {
			 System.out.println(e);
		}  
		
//		PDFExport pdf = new PDFExport();
//		pdf.run();

	}
	
	
	
	@Override
	protected void initialize(String[] args) {
		azienda = args[0];
		inputPath = args[1];
		backupPath = args[2];
		super.initialize(args);
	}



	public static synchronized Application getInstance() {
		return (Application) org.jdesktop.application.Application.getInstance();
	}

	public DatabaseManager getDatabaseManager() {
		return databaseManager;
	}

	public void setDatabaseManager(DatabaseManager databaseManager) {
		this.databaseManager = databaseManager;
	}

	public DAO getDao() {
		return dao;
	}

	public void setDao(DAO dao) {
		this.dao = dao;
	}
	
	@Override
	protected void shutdown() {
 	 		System.exit(1);
	}

	@Override
	protected void startup() {
		init();
	}
	
}
