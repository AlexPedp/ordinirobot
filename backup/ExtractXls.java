package com.pedp.extract.xls;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.pedp.data.Riga;
import com.pedp.testo.Testo;


public class ExtractXls {

	private String deposito;
	private String numero;
	private String data;
	private String consegna;
	private String art;
	private String desc;
	private String mag;
	private String vet;
	private String colli;
	private String type;
	private String rifArt;
	private Riga row;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	public void execute(String inputPath, File file) throws IOException{

		List<Riga> rows = new ArrayList<Riga>();
		 
		FileInputStream inputStream = new FileInputStream(file);
		
		HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
		HSSFSheet  firstSheet = workbook.getSheetAt(0);
		Iterator<Row> iterator = firstSheet.iterator();
		
		type = getType(iterator);

		if (type == null){
			System.out.println("Ordine di cliente non valido");
			return ;
		}
	 
		iterator = firstSheet.iterator();

		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();
			
			if(nextRow.getRowNum() == 4){
				Cell cell1 = nextRow.getCell(26);
				data = getCellValue(cell1, false);
				continue;
			}
			
			if(nextRow.getRowNum() == 11){
					Cell cell1 = nextRow.getCell(21);
					deposito = getCellValue(cell1, false);
					continue;
			}
			
			if(nextRow.getRowNum() == 18){
				
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					
					int index = cell.getColumnIndex();
					switch (index) {
					case 0:
						numero = getCellValue(cell, false);
						break;
					case 9:
						vet = getCellValue(cell, false);
						break;
					case 17:
						consegna = getCellValue(cell, false);
						String[] arr = consegna.split("/");
						String day = String.format("%02d", Integer.parseInt(arr[1]));
						String month = String.format("%02d", Integer.parseInt(arr[0]));
						String year = arr[2];
						consegna = day + "/" + month + "/" + year;						
						break;
					default:
						break;
					}
				}
				
				continue;
			}
  
			
			if(nextRow.getRowNum() < 22){
				continue;  
			}
		 	
				
			while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					int index = cell.getColumnIndex();
					switch (index) {
					case 0:
						if(art==null)
						art = getCellValue(cell, false);
						break;
					case 1:
						if (desc==null)
						desc = getCellValue(cell, false);
						break;
					case 11:
						if (colli==null)
						colli = getCellValue(cell, false);
						break;
					case 19:
						if (mag==null)
						mag = getCellValue(cell, false);
						break;
					case 25:
						if (rifArt==null)
						rifArt = getCellValue(cell, false);
						break;
					default:
						break;
					}
					
//					System.out.println(nextRow.getRowNum() +  "|" + cell.getColumnIndex());
//					String stringCellValue = getCellValue(cell, true);
//					System.out.print(" - ");
				}
			
			if (desc != null && desc.trim().length() > 0){
	 			row = new Riga();
				row.setCliente(type);
				String[] parts = deposito.split("\n");
				row.setSpedizioneName(parts[0]);
				row.setSpedizioneAddress(parts[1]);
				row.setSpedizioneCity(parts[2]);
				row.setNumero(numero);
				row.setData(data);
				row.setConsegna(consegna);
				row.setArticolo(art);
				row.setDescrizione(desc);
				row.setQuantita(colli);
				row.setPrezzo("0.0");
				row.setUm("");
				row.setMagazzino(mag);
				row.setVettore(vet);
				row.setRifArticolo(rifArt);
				rows.add(row);
				clear();
			}
	
		}
		
		inputStream.close();	
		workbook.close();
		
		createCSV(inputPath, file, rows);
		
	}
	
	public void clear(){
		art = null;
		desc = null;
		colli = null;
		mag = null;
		rifArt = null;
	}


	public String getCellValue(Cell cell, boolean debug){
		
		String stringCellValue = null;
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			stringCellValue = cell.getStringCellValue();
			if (debug)
			System.out.print(cell.getStringCellValue());
			break;
		case Cell.CELL_TYPE_BOOLEAN:
			if (debug)
			System.out.print(cell.getBooleanCellValue());
			break;
		case Cell.CELL_TYPE_NUMERIC:
			stringCellValue = null;
			if (HSSFDateUtil.isCellDateFormatted(cell)) {
				stringCellValue = sdf.format(cell.getDateCellValue());
			} else {
				stringCellValue = String.valueOf(cell.getNumericCellValue());    	
			}

			System.out.print(cell.getNumericCellValue());
			break;
		case Cell.CELL_TYPE_FORMULA:
			stringCellValue = null;
			if (HSSFDateUtil.isCellDateFormatted(cell)) {
				stringCellValue = sdf.format(cell.getDateCellValue());
			} else {
				stringCellValue = String.valueOf(cell.getNumericCellValue());    	
			}
			if (debug)
			System.out.print(cell.getNumericCellValue());
			break;
		}
		return stringCellValue;
		
	}
	
	private String getType(Iterator<Row> iterator)  throws IOException{

		String type = null;

		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();
			
			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				
				
				String line = getCellValue(cell, false);
				if (line == null){
					continue;
				}
			
				if (line.toUpperCase().trim().contains(Testo.BERTOZZI)){
					return Testo.BERTOZZI;
				}
				
			}

		}

		return type;

	}
	
	public void createCSV(String inputPath, File file, List<Riga> rows) throws FileNotFoundException{

		String fname = file.getName();
		int pos = fname.lastIndexOf(".");
		if (pos > 0) {
			fname = fname.substring(0, pos);
		}
	 
		File csvFile = new File(inputPath + type + "_" +UUID.randomUUID() + ".csv");

		PrintWriter pw = new PrintWriter(csvFile);
		StringBuilder sb = new StringBuilder();


		for (Riga r: rows) {
			sb.append(r.getCliente().trim());
			sb.append(";");
			sb.append(r.getSpedizioneName().trim());
			sb.append(";");
			sb.append(r.getSpedizioneAddress().trim());
			sb.append(";");
			sb.append(r.getSpedizioneCity().trim());
			sb.append(";");
			sb.append(r.getNumero().trim());
			sb.append(";");
			sb.append(r.getData().trim());
			sb.append(";");
			sb.append(r.getConsegna().trim());
			sb.append(";");
			sb.append(r.getArticolo().trim());
			sb.append(";"); 
			sb.append(r.getDescrizione());
			sb.append(";"); 
			sb.append(r.getQuantita().trim());
			sb.append(";");
			sb.append(r.getPrezzo().trim());
			sb.append(";");
			sb.append(r.getUm());
			sb.append(";");
			sb.append(r.getRifArticolo());
			sb.append(";");
			sb.append(r.getMagazzino());
			sb.append(";");
			sb.append(r.getVettore());
			sb.append(";");
			sb.append("\n");		
		}


		pw.write(sb.toString());
		pw.close();

	}
	
	 

}


