package com.pedp.extract.xlsx;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.UUID;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class ExtractXlsx {


	private final String fileName = "METRO";
	private final String type = "METRO";
	private String deposito = null;
	

	public void execute(File file) throws IOException{

		File csvFile = new File(fileName + "_" +UUID.randomUUID() + ".csv");
		PrintWriter pw = new PrintWriter(csvFile);
		StringBuilder sb = new StringBuilder();
		 
		FileInputStream inputStream = new FileInputStream(file);

		Workbook workbook = new XSSFWorkbook(inputStream);
		Sheet firstSheet = workbook.getSheetAt(0);
		Iterator<Row> iterator = firstSheet.iterator();

		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();
			
			if (deposito == null){
				Cell cell1 = nextRow.getCell(0);
				String[] split = cell1.getStringCellValue().split(" ");
				deposito = split[1];
			}
			
			
			if(nextRow.getRowNum() < 5){
				   continue;  
			}
 
			sb.append(type);
			sb.append(';');
			sb.append(deposito.trim());
			sb.append(';');
			sb.append("");
			sb.append(';');
			sb.append("");
			sb.append(';');
			
			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();

				String stringCellValue = null;
				switch (cell.getCellType()) {
				case Cell.CELL_TYPE_STRING:
					stringCellValue = cell.getStringCellValue();
					
					System.out.print(cell.getStringCellValue());
					break;
				case Cell.CELL_TYPE_BOOLEAN:
					System.out.print(cell.getBooleanCellValue());
					break;
				case Cell.CELL_TYPE_NUMERIC:
					stringCellValue = String.valueOf(cell.getNumericCellValue());
					System.out.print(cell.getNumericCellValue());
					break;
				}
				System.out.print(" - ");
				
				
//				if (stringCellValue!=null){
					sb.append(stringCellValue);
					sb.append(';'); 	
//				}
			}
			
			sb.append('\n');
			System.out.println();
		}


		pw.write(sb.toString());
		pw.close();
		System.out.println("Done");

		workbook.close();
		inputStream.close();
	}




}


